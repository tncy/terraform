provider "gandi" {
  key = var.gandi_api_key
  sharing_id = var.gandi_sharing_id
}

variable "gandi_api_key" {
  type        = string
  description = "The Gandi API key"
}

variable "gandi_sharing_id" {
  type        = string
  description = "The Organisation ID on Gandi platform"
}

data "gandi_domain" "tncy_eu" {
  name = "tncy.eu"
}

resource "gandi_livedns_record" "wildcard" {
  zone = data.gandi_domain.tncy_eu.id
  name = "*"
  type = "CNAME"
  ttl = 300
  values = [
    "${azurerm_public_ip.reverseproxy.domain_name_label}.${var.location}.cloudapp.azure.com."
  ]
}