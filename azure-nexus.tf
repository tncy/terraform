data "azurerm_image" "nexus_image" {
  name                = "${var.prefix}-nexus"
  resource_group_name = data.azurerm_resource_group.images.name
}

resource "azurerm_public_ip" "nexus" {
  name = "${var.prefix}-nexus_pip"
  location = var.location
  resource_group_name = azurerm_resource_group.wil.name
  allocation_method = "Dynamic"
  domain_name_label = "${var.prefix}-nexus"

  tags = {
    project = var.project_name
  }
}

resource "azurerm_network_interface" "nexus" {
  name = "${var.prefix}-nexus_nic"
  location = var.location
  resource_group_name = azurerm_resource_group.wil.name

  ip_configuration {
    name = "ipconfig1"
    subnet_id = azurerm_subnet.wil.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.nexus.id
  }

  tags = {
    project = var.project_name
  }
}

resource "azurerm_virtual_machine" "nexus" {
  name = "${var.prefix}-nexus_vm"
  location = var.location
  resource_group_name = azurerm_resource_group.wil.name

  network_interface_ids = [azurerm_network_interface.nexus.id]
  vm_size = "Standard_D4s_v4"

  delete_os_disk_on_termination = true

  storage_os_disk {
    name = "${var.prefix}-nexus_vm-os_disk"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    id = data.azurerm_image.nexus_image.id
  }

  os_profile {
    computer_name = "${var.prefix}-nexus"
    admin_username = var.admin_user
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.admin_user}/.ssh/authorized_keys"
      key_data = file(var.admin_public_key_path)
    }
  }

  boot_diagnostics {
    enabled = "true"
    storage_uri = azurerm_storage_account.wil.primary_blob_endpoint
  }

  tags = {
    project = var.project_name
  }
}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "nexus" {
  virtual_machine_id = azurerm_virtual_machine.nexus.id
  location           = var.location
  enabled            = true

  daily_recurrence_time = "2359"
  timezone              = "Romance Standard Time"

  notification_settings {
    enabled         = false
  }
}