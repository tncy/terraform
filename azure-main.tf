provider "azurerm" {
  features {}
  #subscription_id = ""
  #tenant_id = ""
  #client_id = ""
  #client_secret = ""
}

provider "random" {

}

data "azurerm_resource_group" "images" {
  name = "Images"
}

resource "azurerm_resource_group" "wil" {
  name     = var.resource_group_name
  location = var.location

  tags = {
    project = var.project_name
  }
}

resource "azurerm_virtual_network" "wil" {
  name                = "${var.prefix}-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = azurerm_resource_group.wil.name

  tags = {
    project = var.project_name
  }
}

resource "azurerm_subnet" "wil" {
  name                 = "default"
  resource_group_name  = azurerm_resource_group.wil.name
  virtual_network_name = azurerm_virtual_network.wil.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_security_group" "wil" {
  name                = "${var.prefix}-nsg"
  location            = var.location
  resource_group_name = azurerm_resource_group.wil.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTP"
    priority                   = 340
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTPS"
    priority                   = 320
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }

  tags = {
    project = var.project_name
  }
}

resource "azurerm_subnet_network_security_group_association" "test" {
  subnet_id                 = azurerm_subnet.wil.id
  network_security_group_id = azurerm_network_security_group.wil.id
}

resource "random_id" "wil" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.wil.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "wil" {
  name                     = "diag${random_id.wil.hex}"
  resource_group_name      = azurerm_resource_group.wil.name
  location                 = "westeurope"
  account_replication_type = "LRS"
  account_tier             = "Standard"

  tags = {
    project = var.project_name
  }
}

resource "azurerm_automation_account" "wil" {
  name                = "${var.prefix}-automation"
  location            = var.location
  resource_group_name = azurerm_resource_group.wil.name
  sku_name            = "Basic"
  tags = {
    project = var.project_name
  }
}