terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.1"
    }
    gandi = {
      source  = "go-gandi/gandi"
      version = ">= 2.1.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">=3.5.1"
    }
  }

  required_version = ">= 1.0"
}