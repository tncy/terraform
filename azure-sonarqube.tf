data "azurerm_image" "sonarqube_image" {
  name                = "${var.prefix}-sonarqube"
  resource_group_name = data.azurerm_resource_group.images.name
}

resource "azurerm_public_ip" "sonarqube" {
  name                = "${var.prefix}-sonarqube_pip"
  location            = var.location
  resource_group_name = azurerm_resource_group.wil.name
  allocation_method   = "Dynamic"
  domain_name_label   = "${var.prefix}-sonarqube"

  tags = {
    project = var.project_name
  }
}

resource "azurerm_network_interface" "sonarqube" {
  name                = "${var.prefix}-sonarqube_nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.wil.name

  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = azurerm_subnet.wil.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.sonarqube.id
  }

  tags = {
    project = var.project_name
  }
}

resource "azurerm_virtual_machine" "sonarqube" {
  name                = "${var.prefix}-sonarqube_vm"
  location            = var.location
  resource_group_name = azurerm_resource_group.wil.name

  network_interface_ids = [azurerm_network_interface.sonarqube.id]
  vm_size               = "Standard_D2s_v4"

  delete_os_disk_on_termination = true

  storage_os_disk {
    name              = "${var.prefix}-sonarqube_vm-os_disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    id = data.azurerm_image.sonarqube_image.id
  }

  os_profile {
    computer_name  = "${var.prefix}-sonarqube"
    admin_username = var.admin_user
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.admin_user}/.ssh/authorized_keys"
      key_data = file(var.admin_public_key_path)
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.wil.primary_blob_endpoint
  }

  tags = {
    project = var.project_name
  }

  connection {
    type  = "ssh"
    host  = "${azurerm_public_ip.sonarqube.domain_name_label}.${var.location}.cloudapp.azure.com"
    user  = var.admin_user
    //private_key = file(var.admin_private_key_path)
    agent = true
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /opt/scripts/provisioning",
      "sudo chgrp adm /opt/scripts/provisioning",
      "sudo chmod g+w /opt/scripts/provisioning"
    ]
  }

  provisioner "file" {
    source      = "provisioning/sonarqube/"
    destination = "/opt/scripts/provisioning"
  }

  provisioner "remote-exec" {
    # TODO: Set env vars SONARQUBE_ADMIN_PWD and SONARQUBE_JENKINS_PWD
    inline = [
      "sudo chown -R root:adm /opt/scripts/provisioning/*",
      "sudo chmod -R ug+x /opt/scripts/provisioning/*.sh",
      "sudo touch /var/log/terraform-provisioning.log",
      "sudo chgrp adm /var/log/terraform-provisioning.log",
      "sudo chmod g+w /var/log/terraform-provisioning.log",
      "sudo /opt/scripts/provisioning/run.sh > /var/log/terraform-provisioning.log",
    ]
    on_failure = continue
  }

}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "sonarqube" {
  virtual_machine_id = azurerm_virtual_machine.sonarqube.id
  location           = var.location
  enabled            = true

  daily_recurrence_time = "2359"
  timezone              = "Romance Standard Time"

  notification_settings {
    enabled = false
  }
}