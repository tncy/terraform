#!/usr/bin/env bash

printf '\n==== Start of Terraform provisioning ====\n\n'

cd "$(dirname "$0")"

# Add Sonarqube users
sed -i -e 's/SONARQUBE_JENKINS_PWD=/SONARQUBE_JENKINS_PWD='"$SONARQUBE_JENKINS_PWD"'/g' /opt/scripts/provisioning/sonarqube_users.sh
sed -i -e 's/SONARQUBE_ADMIN_PWD=/SONARQUBE_ADMIN_PWD='"$SONARQUBE_ADMIN_PWD"'/g' /opt/scripts/provisioning/sonarqube_users.sh
# source ./sonarqube_users.sh

printf '\n==== End of Terraform provisioning ====\n\n'
