#!/usr/bin/env bash

printf '\n==== Start of Terraform provisioning ====\n\n'

cd "$(dirname "$0")"

# Generate Let's Encrypt SSL certificate
source ./lets_encrypt.sh

printf '\n==== End of Terraform provisioning ====\n\n'
