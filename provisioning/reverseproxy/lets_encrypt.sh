#!/usr/bin/env bash

# Install Let's Encrypt certificate
# NB: Certificate creation requires that the Apache config passes the configtest (apache2ctl configtest)
certbot certonly -d mvn.tncy.eu --apache -n -m admin@tncy.eu --agree-tos
certbot certonly -d nexus.tncy.eu --apache -n -m admin@tncy.eu --agree-tos
certbot certonly -d jenkins.tncy.eu --apache -n -m admin@tncy.eu --agree-tos
certbot certonly -d sonarqube.tncy.eu --apache -n -m admin@tncy.eu --agree-tos

systemctl restart apache2