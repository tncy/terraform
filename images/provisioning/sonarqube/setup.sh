#!/usr/bin/env bash

printf '\n==== Start of provisioning ====\n\n'

export DEBIAN_FRONTEND=noninteractive

timedatectl set-timezone Europe/Paris

apt-get update
apt-get -y upgrade
apt-get -y dselect-upgrade

chmod -R +x /tmp/scripts/*.sh

### Requirements setup
source /tmp/scripts/requirements.sh

### Sonarqube server installation
source /tmp/scripts/sonarqube-setup.sh

### Sonarqube configuration
source /tmp/scripts/sonarqube-config.sh

printf '\n==== End of provisioning ====\n\n'
