#!/usr/bin/env bash

# TODO: Split script

printf '\n==== Configuring Sonarqube...\n\n'

### Server config
# PostgreSQL user and database creation
printf '\nCreating database\n'
sudo -u postgres psql -f /tmp/scripts/create_db.sql

printf '\n==== Configuring server...\n\n'
# Database connection
sed -i -e 's/#sonar.jdbc.username=/sonar.jdbc.username=sonarqube/g' /opt/sonarqube/current/conf/sonar.properties
sed -i -e 's/#sonar.jdbc.password=/sonar.jdbc.password=secret/g' /opt/sonarqube/current/conf/sonar.properties
sed -i -e 's/#sonar.jdbc.url=jdbc:postgresql:\/\/localhost\/sonarqube?currentSchema=my_schema/sonar.jdbc.url=jdbc:postgresql:\/\/localhost\/sonarqube/g' /opt/sonarqube/current/conf/sonar.properties

# Activate update center
sed -i -e 's/#sonar.updatecenter/sonar.updatecenter/g' /opt/sonarqube/current/conf/sonar.properties

# Folder paths
sed -i -e 's/#sonar.path.logs=logs/sonar.path.logs=\/var\/local\/sonarqube\/log/g' /opt/sonarqube/current/conf/sonar.properties
sed -i -e 's/#sonar.path.data=data/sonar.path.data=\/var\/local\/sonarqube\/data/g' /opt/sonarqube/current/conf/sonar.properties
sed -i -e 's/#sonar.path.temp=temp/sonar.path.temp=\/var\/local\/sonarqube\/tmp/g' /opt/sonarqube/current/conf/sonar.properties

# Java options
sed -i -e 's/#sonar.web.javaAdditionalOpts=/sonar.web.javaAdditionalOpts=-server/g' /opt/sonarqube/current/conf/sonar.properties
sed -i -e 's/#sonar.ce.javaAdditionalOpts=/sonar.ce.javaAdditionalOpts=-server/g' /opt/sonarqube/current/conf/sonar.properties
sed -i -e 's/#sonar.search.javaAdditionalOpts=/sonar.search.javaAdditionalOpts=-server/g' /opt/sonarqube/current/conf/sonar.properties

### Service settings
source /tmp/scripts/sonarqube-config-service.sh

### Security configuration
# source /tmp/scripts/sonarqube-config-security.sh