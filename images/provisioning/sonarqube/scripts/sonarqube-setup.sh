#!/usr/bin/env bash

### Local environment
printf '\n==== Creating local environment...\n\n'
mkdir -p /var/local/sonarqube/data
useradd -d /var/local/sonarqube -s /bin/false sonarqube
mkdir /var/local/sonarqube/log
mkdir /var/local/sonarqube/tmp
chown -R sonarqube:sonarqube /var/local/sonarqube

### Server installation
printf '\n==== Installing server...\n\n'
# TODO: Check version
export SONARQUBE_VERSION=9.7.0.61563
mkdir /opt/sonarqube
# Original source: https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.7.0.61563.zip
wget -nv https://s3.us-east-2.amazonaws.com/regbuddy.eu/wil/sonarqube-$SONARQUBE_VERSION.zip -P /opt/sonarqube
unzip -q -d /opt/sonarqube /opt/sonarqube/sonarqube-$SONARQUBE_VERSION.zip
# rm /opt/sonarqube/sonarqube-$SONARQUBE_VERSION.zip
ln -s /opt/sonarqube/sonarqube-$SONARQUBE_VERSION /opt/sonarqube/current

chown -R sonarqube:root /opt/sonarqube