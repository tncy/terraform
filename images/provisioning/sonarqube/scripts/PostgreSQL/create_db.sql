-- noinspection SqlNoDataSourceInspectionForFile

CREATE ROLE sonarqube WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  CREATEDB
  CREATEROLE
  REPLICATION
  PASSWORD 'secret';

COMMENT ON ROLE sonarqube IS 'Sonarqube user';

CREATE USER sonarqube WITH PASSWORD 'secret';



CREATE DATABASE sonarqube
    WITH
    OWNER = sonarqube
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

GRANT ALL PRIVILEGES ON DATABASE sonarqube to sonarqube;