#!/usr/bin/env bash

printf '\n==== Configuring security...\n\n'
# Change 'admin' password
curl -X POST -u admin:admin http://localhost:9000/api/users/change_password -d "login=admin&password=$SONARQUBE_ADMIN_PWD&previousPassword=admin"

# Create 'student' user
curl -X POST -u admin:$SONARQUBE_ADMIN_PWD http://localhost:9000/api/users/create -d 'login=student&password=welcome&name=Student&email=student@tncy.eu'

# Create 'jenkins' user
curl -X POST -u admin:$SONARQUBE_ADMIN_PWD http://localhost:9000/api/users/create -d "login=jenkins&password=$SONARQUBE_JENKINS_PWD&name=Jenkins&email=jenkins@tncy.eu"

# Create 'jenkins' token
curl -X POST -u admin:$SONARQUBE_ADMIN_PWD http://localhost:9000/api/user_tokens/generate -d "login=jenkins&name=Maven" > /var/local/sonarqube/jenkins_token.json

