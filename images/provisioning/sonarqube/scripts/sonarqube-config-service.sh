#!/usr/bin/env bash

### Service setup
printf '\n==== Enabling service...\n\n'
sed -i -e 's/sonarqube-XXX/sonarqube-'$SONARQUBE_VERSION'/g' /tmp/conf/sonarqube.service
sed -i -e 's/sonar-application-XXX.jar/sonar-application-'$SONARQUBE_VERSION.jar'/g' /tmp/conf/sonarqube.service
mv /tmp/conf/sonarqube.service /etc/systemd/system/sonarqube.service

# TODO: Check requirements
# See: https://docs.sonarsource.com/sonarqube/9.9/requirements/prerequisites-and-overview/#platform-notes
# echo 'vm.max_map_count=262144' >> /etc/sysctl.conf
# systemctl daemon-reload
echo "vm.max_map_count=262144" | tee /etc/sysctl.d/99-sonarqube.conf
echo "fs.file-max=65536" | tee -a /etc/sysctl.d/99-sonarqube.conf

systemctl enable sonarqube


### Start service
#printf '\n==== Starting service...\n\n'
#
#
#
#counter=0
#until [ $counter -gt 60 ]
#do
#  echo Counter: $counter
#  if [ $(systemctl is-active sonarqube) == "active" ]; then
#      systemctl status sonarqube.service
#      printf 'Sonarqube is active\n'
#      break
#  fi
#  printf 'Sonarqube is not started yet\n'
#  sleep 2
#done
#
#counter=0
#until [ $counter -gt 240 ]
#do
#  echo Counter: $counter
#  curl -X GET --head --fail http://localhost:9000
#  ((counter++))
#  sleep 2
#done
#
#
#STATUS=$(curl -X GET -u admin:admin http://localhost:9000/api/system/status)
#echo "$STATUS"
#echo $STATUS
#
#
#
#
#### Get server version
#curl -X GET -u admin:admin http://localhost:9000/api/server/version
#
#### Get server health
#curl -X GET -u admin:admin http://localhost:9000/api/system/health