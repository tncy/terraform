#!/usr/bin/env bash

printf '\n==== Installing requirements...\n\n'
# See: https://docs.sonarsource.com/sonarqube/9.9/requirements/prerequisites-and-overview/

# OpenJDK 11, unzip
apt-get -qq -y install openjdk-11-jdk unzip



# PostgreSQL Server
apt-get -qq -y install postgresql