#!/bin/bash

if [ ! -d "/var/local/jenkins/out/views" ];then
	mkdir -p /var/local/jenkins/out/views
fi

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli -webSocket get-view "$1" > /var/local/jenkins/out/views/$2.xml