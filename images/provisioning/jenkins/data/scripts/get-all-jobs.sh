#!/bin/bash

if [ ! -d "/var/local/jenkins/out/jobs" ];then
	mkdir -p /var/local/jenkins/out/jobs
fi

/var/local/jenkins/scripts/get-job.sh "XRO - demo-validation" demo-validation
/var/local/jenkins/scripts/get-job.sh "XRO - demo-validation (site)" demo-validation_site

/var/local/jenkins/scripts/get-job.sh "XRO - my-docker1" my-docker1
/var/local/jenkins/scripts/get-job.sh "XRO - my-project1" my-project1

/var/local/jenkins/scripts/get-job.sh "XRO - tncy" tncy
/var/local/jenkins/scripts/get-job.sh "XRO - tncy (site)" tncy_site

/var/local/jenkins/scripts/get-job.sh "XRO - tncy-bm" tncy-bm
/var/local/jenkins/scripts/get-job.sh "XRO - tncy-bm (QA)" tncy-bm_qa
/var/local/jenkins/scripts/get-job.sh "XRO - tncy-bm (site)" tncy-bm_site

/var/local/jenkins/scripts/get-job.sh "XRO - validation-constraints" validation-constraints
/var/local/jenkins/scripts/get-job.sh "XRO - validation-constraints (QA)" validation-constraints_qa
/var/local/jenkins/scripts/get-job.sh "XRO - validation-constraints (site)" validation-constraints_site