#!/bin/bash

if [ ! -d "/var/local/jenkins/out/jobs" ];then
	mkdir -p /var/local/jenkins/out/jobs
fi

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli get-job "$1" > /var/local/jenkins/out/jobs/$2.xml
