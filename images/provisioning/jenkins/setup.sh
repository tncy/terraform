#!/usr/bin/env bash

printf '\n==== Start of provisioning ====\n\n'

export DEBIAN_FRONTEND=noninteractive
export NEEDRESTART_MODE=a
export APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1

timedatectl set-timezone Europe/Paris

apt-get -qq update
apt-get -qq -y upgrade
apt-get -qq -y dselect-upgrade

chmod -R +x /tmp/scripts/*.sh

### Requirements setup
source /tmp/scripts/requirements.sh

### Jenkins Server installation
source /tmp/scripts/jenkins-setup.sh

### Build tools installation
source /tmp/scripts/build_tools.sh

### Jenkins server configuration
source /tmp/scripts/jenkins-config.sh

## Management script installation
printf '\n>>> Copying management scripts...\n'
mkdir -p /opt/scripts/jenkins
mv /tmp/data/scripts/*.sh /opt/scripts/jenkins
# Remove Windows Carriage Return from scripts
sed -i -e 's/\r$//' /opt/scripts/jenkins/*.sh
chmod +x /opt/scripts/jenkins/*.sh

printf '\n==== End of provisioning ====\n\n'