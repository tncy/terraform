#!/usr/bin/env bash

wget -q -O - https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
apt-get update

printf '\n...Packer\n'
apt-get install packer

printf '\n...Terraform\n'
apt-get install terraform

printf '\n...Vault\n'
apt-get install vault
