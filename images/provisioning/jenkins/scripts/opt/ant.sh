#!/usr/bin/env bash

printf '\n...Ant\n'
mkdir /opt/ant
wget -q https://s3.us-east-2.amazonaws.com/regbuddy.eu/wil/apache-ant-1.10.13-bin.tar.gz -P /opt/ant
tar -xzf /opt/ant/apache-ant-1.10.13-bin.tar.gz -C /opt/ant
rm /opt/ant/apache-ant-1.10.13-bin.tar.gz
ln -s /opt/ant/apache-ant-1.10.13 /opt/ant/current