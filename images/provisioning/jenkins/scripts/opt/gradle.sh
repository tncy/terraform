#!/usr/bin/env bash

printf '\n...Gradle\n'
mkdir /opt/gradle
wget -q https://s3.us-east-2.amazonaws.com/regbuddy.eu/wil/gradle-8.2.1-bin.zip -P /opt/gradle
unzip -q -d /opt/gradle /opt/gradle/gradle-8.2.1-bin.zip
rm /opt/gradle/gradle-8.2.1-bin.zip
ln -s /opt/gradle/gradle-8.2.1 /opt/gradle/current
