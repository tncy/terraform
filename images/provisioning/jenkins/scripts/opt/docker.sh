#!/usr/bin/env bash

# See: https://docs.docker.com/engine/install/ubuntu/

printf '\n...Docker\n'

# Install packages to allow apt to use a repository over HTTPS
apt-get -qq -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
apt-get -qq -y install ca-certificates curl gnupg

#Add Docker’s official GPG key
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
chmod a+r /etc/apt/keyrings/docker.gpg

# Set up the stable repository
echo  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" |  tee /etc/apt/sources.list.d/docker.list > /dev/null

# Update the apt package index.
apt-get -qq update

# Install the latest version of Docker Engine - Community and containerd
apt-get -qq -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Verify that Docker Engine - Community is installed correctly by running the hello-world image.
#docker run hello-world

# Set permission to manage Docker for non-root users
# See: https://docs.docker.com/engine/install/linux-postinstall/
# groupadd docker
usermod -aG docker jenkins
#CHECK: Obsolete ?
#useradd -m -s /bin/bash -G docker hudson
#newgrp docker
#DOCKER_INSTALL
printf '\n...Docker done!\n'