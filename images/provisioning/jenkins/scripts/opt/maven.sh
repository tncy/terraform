#!/usr/bin/env bash

printf '\n...Maven\n'
mkdir /opt/maven
wget -q https://s3.us-east-2.amazonaws.com/regbuddy.eu/wil/apache-maven-3.9.4-bin.tar.gz -P /opt/maven
tar -xzf /opt/maven/apache-maven-3.9.4-bin.tar.gz -C /opt/maven
rm /opt/maven/apache-maven-3.9.4-bin.tar.gz
ln -s /opt/maven/apache-maven-3.9.4 /opt/maven/current

# Maven settings
mkdir -p /var/local/jenkins/.m2/repository
mv /tmp/conf/opt/maven-settings.xml /var/local/jenkins/.m2/settings.xml
mv /tmp/conf/opt/maven-settings-security.xml /var/local/jenkins/.m2/settings-security.xml
chown -R jenkins:jenkins /var/local/jenkins/.m2
sudo -u jenkins ln -s /var/local/jenkins/.m2 /var/lib/jenkins/.m2
