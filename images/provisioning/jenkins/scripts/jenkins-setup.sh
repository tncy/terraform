#!/usr/bin/env bash

### Local environment
printf '\n==== Installing Jenkins server...\n\n'
# See: https://www.jenkins.io/doc/book/installing/linux/#debianubuntu
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | tee /etc/apt/sources.list.d/jenkins.list > /dev/null
apt-get -qq update
apt-get -qq -y install jenkins

### Local environment
printf '\n==== Creating local environment...\n\n'
# SSH Settings
#TODO: Review Bitkucket SSH key encryption
mkdir -p /var/lib/jenkins/.ssh
chmod 700 /var/lib/jenkins/.ssh
cp /tmp/ssh_keys/rsa/Jenkins#WIL_rsa /var/lib/jenkins/.ssh/id_rsa
cp /tmp/ssh_keys/rsa/Jenkins#WIL_rsa.pub /var/lib/jenkins/.ssh/id_rsa.pub
cp /tmp/ssh_keys/ed25519/Jenkins#WIL_ed25519 /var/lib/jenkins/.ssh/id_ed25519
cp /tmp/ssh_keys/ed25519/Jenkins#WIL_ed25519.pub /var/lib/jenkins/.ssh/id_ed25519.pub
chmod 500 /var/lib/jenkins/.ssh/id_*
ssh-keyscan -t rsa -H bitbucket.org >> /var/lib/jenkins/.ssh/known_hosts
ssh-keyscan -t ed25519 -H bitbucket.org >> /var/lib/jenkins/.ssh/known_hosts
chown -R jenkins:jenkins /var/lib/jenkins/.ssh
sudo -u jenkins ssh -T git@bitbucket.org