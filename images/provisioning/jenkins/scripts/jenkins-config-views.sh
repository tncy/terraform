#!/usr/bin/env bash

## Views
printf '\n>>> Deploying views...\n'

mkdir -p /var/local/jenkins/views
cp /tmp/conf/views/* /var/local/jenkins/views/

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli -webSocket create-view < /var/local/jenkins/views/lts.xml
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli -webSocket create-view < /var/local/jenkins/views/xro.xml
