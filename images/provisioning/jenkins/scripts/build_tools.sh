#!/usr/bin/env bash

printf '\n==== Installing build tools...\n\n'

ls -al .

## Apache Ant
source /tmp/scripts/opt/ant.sh

## Apache Maven 3
source /tmp/scripts/opt/maven.sh

## Gradle
source /tmp/scripts/opt/gradle.sh

## Docker
source /tmp/scripts/opt/docker.sh

## HashiCorp Tools: Packer, Terraform & Vault
source /tmp/scripts/opt/hashicorp.sh