#!/usr/bin/env bash

printf '\n==== Configuring Jenkins...\n\n'

## Jenkins CLI
# See: https://jenkins.io/doc/book/managing/cli/
printf '\n>>> Installing Jenkins CLI...\n'
wget -q http://localhost:8080/jnlpJars/jenkins-cli.jar -P /var/local/jenkins/

## Jenkins CLI auth file
JENKINS_ADMIN_PWD=$(< /var/lib/jenkins/secrets/initialAdminPassword)
echo "admin:$JENKINS_ADMIN_PWD" > /var/local/jenkins/.jenkins-cli

## Install plugins
source /tmp/scripts/jenkins-config-plugins.sh

## Create users
source /tmp/scripts/jenkins-config-users.sh

## Update server config
printf '\n>>> Updating server config...\n'
sed -i -e 's/<installStateName>NEW</<installStateName>RUNNING</g' /var/lib/jenkins/config.xml
sed -i -e 's/<disableSignup>true</<disableSignup>false</g' /var/lib/jenkins/config.xml

## Configure system including build tools, credentials
source /tmp/scripts/jenkins-config-jcasc.sh

## Configure jobs
source /tmp/scripts/jenkins-config-jobs.sh

## Configure views
source /tmp/scripts/jenkins-config-views.sh

## Finalize configuration
# see: https://docs.cloudbees.com/docs/cloudbees-ci-kb/latest/client-and-managed-masters/execute-groovy-with-a-rest-call
java -jar /var/local/jenkins/jenkins-cli.jar -s "http://localhost:8080" -auth @/var/local/jenkins/.jenkins-cli groovy = < /var/local/jenkins/jcasc/install-state.groovy
