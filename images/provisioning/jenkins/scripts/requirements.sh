#!/usr/bin/env bash

printf '\n==== Installing requirements...\n\n'

apt-get -qq -y install openjdk-8-jdk
apt-get -qq -y install openjdk-11-jdk
apt-get -qq -y install openjdk-17-jdk
apt-get -qq -y install unzip