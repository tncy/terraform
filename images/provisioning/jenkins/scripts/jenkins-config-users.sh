#!/usr/bin/env bash

## Create users
printf '\n>>> Creating users...\n'

echo "jenkins.model.Jenkins.instance.securityRealm.createAccount(\"$JENKINS_USER_NAME\", \"$JENKINS_USER_PWD\")" | java -jar /var/local/jenkins/jenkins-cli.jar -s "http://localhost:8080" -auth @/var/local/jenkins/.jenkins-cli groovy =

echo "jenkins.model.Jenkins.instance.securityRealm.createAccount(\"student\", \"welcome\")" | java -jar /var/local/jenkins/jenkins-cli.jar -s "http://localhost:8080" -auth @/var/local/jenkins/.jenkins-cli groovy =
