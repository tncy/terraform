#!/usr/bin/env bash

## JCasC
# See: https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/README.md

printf '\n>>> Applying configuration as code ...\n'

mkdir -p /var/local/jenkins/jcasc
cp /tmp/conf/casc/* /var/local/jenkins/jcasc/

sed -i -e 's/password: "NEXUS_JENKINS_PWD"/password: "'"$NEXUS_JENKINS_PWD"'"/g' /var/local/jenkins/jcasc/credentials.yaml
sed -i -e 's/password: "BITBUCKET_JENKINS_PWD"/password: "'"$BITBUCKET_JENKINS_PWD"'"/g' /var/local/jenkins/jcasc/credentials.yaml
sed -i -e 's/passphrase: "JENKINS_PRIVATE_KEY_PASSPHRASE"/passphrase: "'"$JENKINS_PRIVATE_KEY_PASSPHRASE"'"/g' /var/local/jenkins/jcasc/credentials.yaml

sed -i -e 's/password: "EMAIL_PWD"/password: "'"$EMAIL_PWD"'"/g' /var/local/jenkins/jcasc/unclassified.yaml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli apply-configuration < /var/local/jenkins/jcasc/unclassified.yaml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli apply-configuration < /var/local/jenkins/jcasc/tools.yaml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli apply-configuration < /var/local/jenkins/jcasc/credentials.yaml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli apply-configuration < /var/local/jenkins/jcasc/authorisations.yaml

