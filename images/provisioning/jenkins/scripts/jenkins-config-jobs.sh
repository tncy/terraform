#!/usr/bin/env bash

## Jobs
printf '\n>>> Deploying jobs...\n'

mkdir -p /var/local/jenkins/jobs
cp /tmp/conf/jobs/*.xml /var/local/jenkins/jobs/

# Instructor's projects
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - demo-validation' < /var/local/jenkins/jobs/demo-validation.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - demo-validation (site)' < /var/local/jenkins/jobs/demo-validation_site.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - my-project1' < /var/local/jenkins/jobs/my-project1.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - tncy-bm' < /var/local/jenkins/jobs/tncy-bm.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - tncy-bm (site)' < /var/local/jenkins/jobs/tncy-bm_site.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - tncy-bm (QA)' < /var/local/jenkins/jobs/tncy-bm_qa.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'XRO - my-docker1' < /var/local/jenkins/jobs/my-docker1.xml


### Jobs for reusable libraries
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'validation-constraints' < /var/local/jenkins/jobs/validation-constraints.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'validation-constraints (site)' < /var/local/jenkins/jobs/validation-constraints_site.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'validation-constraints (QA)' < /var/local/jenkins/jobs/validation-constraints_qa.xml


### TNCY LTS jobs
# Corporate and projects governance
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'tncy' < /var/local/jenkins/jobs/tncy.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'tncy (site)' < /var/local/jenkins/jobs/tncy_site.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'gla' < /var/local/jenkins/jobs/gla.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'gla (site)' < /var/local/jenkins/jobs/gla_site.xml

# Archetypes
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'tncy-quickstart-archetype' < /var/local/jenkins/jobs/tncy-quickstart-archetype.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'tncy-quickstart-archetype (site)' < /var/local/jenkins/jobs/tncy-quickstart-archetype_site.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'gla-quickstart-archetype' < /var/local/jenkins/jobs/gla-quickstart-archetype.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'gla-quickstart-archetype (site)' < /var/local/jenkins/jobs/gla-quickstart-archetype_site.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'gla-webapp-archetype' < /var/local/jenkins/jobs/gla-webapp-archetype.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'gla-webapp-archetype (site)' < /var/local/jenkins/jobs/gla-webapp-archetype_site.xml

# Plugins
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'helper-maven-plugin' < /var/local/jenkins/jobs/tncy.xml

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli create-job 'helper-maven-plugin (site)' < /var/local/jenkins/jobs/tncy_site.xml

