#!/usr/bin/env bash

printf '\n>>> Deploying plugins...\n'

# Folders : https://plugins.jenkins.io/cloudbees-folder/
# OWASP Markup Formatter : https://plugins.jenkins.io/antisamy-markup-formatter/
# Credentials Binding : https://plugins.jenkins.io/credentials-binding/
# Build Timeout : https://plugins.jenkins.io/build-timeout/
# Workspace Cleanup : https://plugins.jenkins.io/ws-cleanup/
# Timestamper : https://plugins.jenkins.io/timestamper/
# Pipeline : https://plugins.jenkins.io/workflow-aggregator/
# Pipeline: Stage View : https://plugins.jenkins.io/pipeline-stage-view/
# SSH Build Agents : https://plugins.jenkins.io/ssh-slaves/
# Email Extension : https://plugins.jenkins.io/email-ext/
# Mailer : https://plugins.jenkins.io/mailer/


java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin git -deploy
printf '\n...Git Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin maven-plugin -deploy
printf '\n...Maven Integration Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin m2release -deploy
printf '\n...M2 Release Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin ant -deploy
printf '\n...Ant Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin gradle -deploy
printf '\n...Gradle Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin docker-plugin -deploy
printf '\n...Docker Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin authorize-project -deploy
printf '\n...Authorize Project Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin workflow-aggregator -deploy
printf '\n...Workflow Aggregator Plugin deployed\n'

java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin configuration-as-code -deploy
printf '\n...Configuration as code Plugin deployed\n'

# See: https://plugins.jenkins.io/matrix-auth/
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin matrix-auth -deploy
printf '\n...Matrix Authorization Strategy deployed\n'

# See: https://plugins.jenkins.io/docker-workflow/
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin docker-workflow -deploy
printf '\n...Docker Pipeline deployed\n'

# See: https://plugins.jenkins.io/ssh-agent/
java -jar /var/local/jenkins/jenkins-cli.jar -s http://localhost:8080/ -auth @/var/local/jenkins/.jenkins-cli install-plugin ssh-agent -deploy
printf '\n...SSH Agent Plugin deployed\n'

# Restarts server to enabled installed plugins
# - Matrix Authorization Strategy
printf '\nRestarting Jenkins service\n'
systemctl restart jenkins
sleep 10

counter=0
until [ $counter -gt 60 ]
do
  echo Counter: $counter
  if [ $(systemctl is-active jenkins) == "active" ]; then
      printf 'Jenkins is active\n'
      break
  fi
  printf 'Jenkins not started\n'
  sleep 2
done