#!/usr/bin/env bash

printf '\n==== Building Maven hosted site...\n\n'

# User credentials creation
printf '\nCreating user credentials...\n'
mkdir -p /var/local/apache2
htpasswd -cb /var/local/apache2/auth student welcome
htpasswd -cb /var/local/apache2/auth jenkins welcome
chown www-data:www-data /var/local/apache2/auth

# Content preparation
printf '\nBuilding site content...\n'
mv /tmp/data/sites /var/local/apache2
mkdir -p /var/local/apache2/sites/maven/sites
mkdir -p /var/local/apache2/sites/maven/releases
mkdir -p /var/local/apache2/sites/maven/snapshots
mkdir -p /var/local/apache2/sites/maven/extras
chown -R root:www-data /var/local/apache2/sites

# Permission setup
#chgrp -R www-data /var/local/apache2/sites/maven/*
chmod g+w /var/local/apache2/sites/maven/*