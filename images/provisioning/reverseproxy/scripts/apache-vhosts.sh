#!/usr/bin/env bash

printf '\n==== Configuring virtual hosts...\n\n'

# Generates self-signed certificate for Apache SSL virtual hosts
sudo openssl req -x509 -noenc -days 365 -newkey rsa:2048 -subj "/CN=*.tncy.eu" -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt

# Installs vhost configuration fragment
printf '\nConfiguring vhosts...\n'
mkdir /etc/apache2/inc
mv /tmp/conf/fragments/* /etc/apache2/inc/
chown -R root:root /etc/apache2/inc

# Installs then enables virtual hosts
mv /tmp/conf/vhosts/*.conf /etc/apache2/sites-available/
chown root:root /etc/apache2/sites-available/

sed -i -e 's/Define PREFIX wil/Define PREFIX '"$PREFIX"'/g' /etc/apache2/sites-available/*.conf

printf '\Enabling vhosts...\n'
cd /etc/apache2/sites-available
a2ensite maven* nexus* jenkins* sonarqube*
#cd /etc/apache2/sites-available & find . -type f -and -not -name "*default*" -exec a2ensite {} \;

printf '\nReloading Apache configuration...\n'
systemctl reload apache2