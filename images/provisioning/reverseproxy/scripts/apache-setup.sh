#!/usr/bin/env bash

printf '\n==== Installing server...\n\n'
# Installs Apache 2 HTTP Server
apt-get -y install apache2
# Installs Certbot for Let's Encrypt certificate management
apt-get -y install certbot python3-certbot-apache

# Enables required Apache 2 modules
a2enmod dav_fs
a2enmod dav_lock
a2enmod headers
a2enmod http2
a2enmod proxy
a2enmod proxy_http
a2enmod rewrite
a2enmod ssl

### Overwrite module configuration
mv /tmp/conf/mods/* /etc/apache2/mods-available/
chown -R root:root /etc/apache2/mods-available/

### Start service
printf '\nRestarting Apache service...\n\n'
service apache2 restart