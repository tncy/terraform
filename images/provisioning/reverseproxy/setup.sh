#!/usr/bin/env bash

printf '\n==== Start of provisioning ====\n\n'

export DEBIAN_FRONTEND=noninteractive

timedatectl set-timezone Europe/Paris

apt-get update
apt-get -y upgrade
apt-get -y dselect-upgrade

chmod -R +x /tmp/scripts/*.sh

### Requirements setup
source /tmp/scripts/requirements.sh

### Apache HTTP server installation
source /tmp/scripts/apache-setup.sh

### Maven hosted site preparation
source /tmp/scripts/mvn-site.sh

### Apache HTTP server installation
source /tmp/scripts/apache-vhosts.sh

printf '\n==== End of provisioning ====\n\n'