#!/usr/bin/env bash

echo "Add roles"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-ci.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-maven-dev.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-maven-rm.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-maven-lts-rm.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-python-dev.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-python-rm.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-docker-dev.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-docker-rm.json
sleep 5

curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/roles -d @/tmp/conf/api/role-spy.json
sleep 5
printf '\n===================\n\n'

echo "Add users"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/users -d @/tmp/conf/api/user-student.json
sleep 5

sed -i -e 's/\"password\": \"\"/\"password\": \"'"$NEXUS_JENKINS_PWD"'\"/g' /tmp/conf/api/user-jenkins.json
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/users -d @/tmp/conf/api/user-jenkins.json
sleep 5
printf '\n===================\n\n'

echo "Change 'admin' password"
curl -v -X PUT -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: text/plain" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/users/admin/change-password -d $NEXUS_ADMIN_PWD
sleep 5
printf '\n===================\n\n'