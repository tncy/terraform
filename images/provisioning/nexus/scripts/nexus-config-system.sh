#!/usr/bin/env bash

echo "Enable 'anonymous' access"
curl -v -X PUT -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/security/anonymous -d @/tmp/conf/api/anonymous.json
sleep 5
#curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: text/plain" http://localhost:8081/service/rest/v1/script/anonymous/run
#sleep 5
printf '\n===================\n\n'

echo "Set base URL"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" http://localhost:8081/service/rest/v1/script -d @/tmp/conf/api/groovy/base-url.json
sleep 5
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: text/plain" http://localhost:8081/service/rest/v1/script/baseURL/run
sleep 5
printf '\n===================\n\n'

echo "Email configuration"
sed -i -e 's/\"password\": \"\"/\"password\": \"'"$EMAIL_PWD"'\"/g' /tmp/conf/api/email-settings.json
more
curl -v -X PUT -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8081/service/rest/v1/email -d @/tmp/conf/api/email-settings.json
sleep 5
printf '\n===================\n\n'