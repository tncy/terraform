#!/usr/bin/env bash

printf '\n==== Installing requirements...\n\n'
# See: https://help.sonatype.com/repomanager3/product-information/sonatype-nexus-repository-system-requirements
# OpenJDK 8
apt-get -qq -y install openjdk-8-jdk