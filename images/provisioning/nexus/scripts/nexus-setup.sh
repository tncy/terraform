#!/usr/bin/env bash

### Local environment
printf '\n==== Creating local environment for Nexus Repository Manager server...\n\n'
mkdir -p /var/local/nexus
useradd -d /var/local/nexus -s /bin/false nexus
mkdir /var/local/nexus/log
mkdir /var/local/nexus/tmp
chown -R nexus:nexus /var/local/nexus

#### Server installation
printf '\n==== Installing Nexus Repository Manager server...\n\n'
mkdir /opt/nexus
wget -nv https://s3.us-east-2.amazonaws.com/regbuddy.eu/wil/nexus-3.59.0-01-unix.tar.gz -P /opt/nexus*
tar -xzf /opt/nexus/nexus-3.59.0-01-unix.tar.gz -C /opt/nexus/
rm /opt/nexus/nexus-3.59.0-01-unix.tar.gz
ln -s /opt/nexus/nexus-3.59.0-01 /opt/nexus/current

# Fixme
mv /opt/nexus/sonatype-work/nexus3/orient/plugins /var/local/nexus/orient/

rm -r /opt/nexus/sonatype-work/

chown -R nexus:root /opt/nexus

### Server config
printf '\nConfiguring server JVM...\n'
# sed -i -e 's/Xms2703m/Xms1024m/g' /opt/nexus/current/bin/nexus.vmoptions
# sed -i -e 's/Xmx2703m/Xmx1024m/g' /opt/nexus/current/bin/nexus.vmoptions
sed -i -e 's/\.\.\/sonatype-work\/nexus3/\/var\/local\/nexus/g' /opt/nexus/current/bin/nexus.vmoptions

printf '\nDefining run as user...\n'
sed -i -e 's/#run_as_user=""/run_as_user="nexus"/g' /opt/nexus/current/bin/nexus.rc

### Enable service
printf '\n==== Enabling Nexus Repository Manager service...\n\n'
mv /tmp/conf/nexus.service /etc/systemd/system/nexus.service

systemctl daemon-reload
systemctl enable nexus.service

# N.B. We need to start service first to generate the default nexus.properties file
printf '\n==== Starting Nexus Repository Manager service...\n'
systemctl start nexus.service

# shellcheck disable=SC2091
until $(curl --output /dev/null --silent --head --fail http://localhost:8081); do
    printf '.'
    sleep 2
done
printf '\n'

# Enable adding and editing scripts
printf '\nEnabling Groovy script adding and editing...\n'
# See: https://issues.sonatype.org/browse/NEXUS-23205

mv /var/local/nexus/etc/nexus.properties /var/local/nexus/etc/nexus.properties.orig
echo nexus.scripts.allowCreation=true > /var/local/nexus/etc/nexus.properties

printf '\n==== Restarting Nexus Repository Manager service...\n'
systemctl restart nexus.service

# shellcheck disable=SC2091
until $(curl --output /dev/null --silent --head --fail http://localhost:8081); do
    printf '.'
    sleep 2
done
printf '\n'