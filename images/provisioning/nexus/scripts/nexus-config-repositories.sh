#!/usr/bin/env bash

echo "Create Amazon S3 blobstore"
sed -i -e 's/\"accessKeyId\": \"\"/\"accessKeyId\": \"'"$AWS_ACCESS_KEY_ID"'\"/g' /tmp/conf/api/blobstore-s3.json
# We use '|' as separator because $AWS_ACCESS_KEY_SECRET usually contains '/' characters.
sed -i -e  's|\"secretAccessKey\": \"\"|\"secretAccessKey\": \"'"$AWS_ACCESS_KEY_SECRET"'\"|g' /tmp/conf/api/blobstore-s3.json
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/blobstores/s3 -d @/tmp/conf/api/blobstore-s3.json
sleep 5
printf '\n===================\n\n'

echo "Create Maven LTS repository"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/repositories/maven/hosted -d @/tmp/conf/api/repository-maven-lts-releases.json
sleep 5
printf '\n===================\n\n'

echo "Add Maven LTS repository to Maven Public repository group"
curl -v PUT -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/repositories/maven/group/maven-public -d @/tmp/conf/api/repository-maven-public.json
sleep 5
printf '\n===================\n\n'

echo "Create PyPi Releases repository"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/repositories/pypi/hosted -d @/tmp/conf/api/repository-pypi-releases.json
sleep 5
printf '\n===================\n\n'

echo "Create PyPi Test repository"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/repositories/pypi/hosted -d @/tmp/conf/api/repository-pypi-test.json
sleep 5
printf '\n===================\n\n'

echo "Create Docker Releases repository"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/repositories/docker/hosted -d @/tmp/conf/api/repository-docker-releases.json
sleep 5
printf '\n===================\n\n'

echo "Create Docker Test repository"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" -H "accept: application/json" http://localhost:8081/service/rest/v1/repositories/docker/hosted -d @/tmp/conf/api/repository-docker-test.json
sleep 5
printf '\n===================\n\n'