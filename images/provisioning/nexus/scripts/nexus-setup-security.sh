#!/usr/bin/env bash

printf '\n==== Enforcing Nexus Repository Manager server security...\n\n'
printf '\nDisabling Groovy script adding and editing...\n\n'
rm /var/local/nexus/etc/nexus.properties
mv /var/local/nexus/etc/nexus.properties.orig /var/local/nexus/etc/nexus.properties