#!/usr/bin/env bash

printf '\n==== Configuring Nexus Repository Manager...\n\n'

NEXUS_USERNAME="admin"
NEXUS_PASSWORD=$(</var/local/nexus/admin.password)

echo "Groovy script Hello World"
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: application/json" http://localhost:8081/service/rest/v1/script -d @/tmp/conf/api/groovy/helloworld.json
sleep 5
curl -v POST -u $NEXUS_USERNAME:$NEXUS_PASSWORD -H "Content-Type: text/plain" http://localhost:8081/service/rest/v1/script/helloworld/run
sleep 5
printf '\n===================\n\n'

### System configuration
source /tmp/scripts/nexus-config-system.sh

### Repository configuration
source /tmp/scripts/nexus-config-repositories.sh

### Security configuration
source /tmp/scripts/nexus-config-security.sh