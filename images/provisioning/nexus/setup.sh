#!/usr/bin/env bash

printf '\n==== Start of provisioning ====\n\n'

export DEBIAN_FRONTEND=noninteractive

timedatectl set-timezone Europe/Paris

apt-get update
apt-get -y upgrade
apt-get -y dselect-upgrade

chmod -R +x /tmp/scripts/*.sh

### Requirements setup
source /tmp/scripts/requirements.sh

### Nexus Repository Manager server installation
source /tmp/scripts/nexus-setup.sh

### Nexus Repository Manager configuration
source /tmp/scripts/nexus-config.sh

### Nexus Repository Manager server installation
source /tmp/scripts/nexus-setup-security.sh

printf '\n==== End of provisioning ====\n\n'