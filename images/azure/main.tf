provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "images" {
  name = var.resource_group_name
  location = var.location

  tags = {
    project = var.project_name
  }
}
