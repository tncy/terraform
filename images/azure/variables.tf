variable "project_name" {
  type = string
  description = "The name that we will use as the 'project' tag value"
  default = "Workshop IL"
}

variable "location" {
  type = string
  description = "The location where resources are created"
  default = "westeurope"
}

variable "resource_group_name" {
  type = string
  description = "The name of the resource group in which the image resources are created"
  default = "images"
}