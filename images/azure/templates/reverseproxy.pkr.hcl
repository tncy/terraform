source "azure-arm" "reverse-proxy" {
  azure_tags = {
    Project = var.project_name
  }
  client_id                         = var.azure_client_id
  client_secret                     = var.azure_client_secret
  image_offer                       = "0001-com-ubuntu-server-jammy"
  image_publisher                   = "Canonical"
  image_sku                         = "22_04-lts-gen2"
  location                          = var.location
  managed_image_name                = "${var.prefix}-reverseproxy"
  managed_image_resource_group_name = var.images_resource_group_name
  os_type                           = "Linux"
  subscription_id                   = var.azure_subscription_id
  tenant_id                         = var.azure_tenant_id
  vm_size                           = "Standard_B1ms"
}

build {
  name= "wil"
  sources = ["source.azure-arm.reverse-proxy"]

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/reverseproxy/conf"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/reverseproxy/data"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/reverseproxy/scripts"
  }

  provisioner "shell" {
    environment_vars = ["PREFIX=${var.prefix}-"]
    execute_command  = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "../../provisioning/reverseproxy/setup.sh"
  }

}
