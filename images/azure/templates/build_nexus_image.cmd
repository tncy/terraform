@echo off
cls

set PACKER_LOG=1
set PACKER_LOG_PATH=packer.log
del /Q %PACKER_LOG_PATH%

@echo on
packer build -only=wil.azure-arm.nexus .