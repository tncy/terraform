source "azure-arm" "jenkins" {
  azure_tags = {
    Project = var.project_name
  }
  client_id                         = var.azure_client_id
  client_secret                     = var.azure_client_secret
  image_offer                       = "0001-com-ubuntu-server-jammy"
  image_publisher                   = "Canonical"
  image_sku                         = "22_04-lts-gen2"
  location                          = "${var.location}"
  managed_image_name                = "${var.prefix}-jenkins"
  managed_image_resource_group_name = var.images_resource_group_name
  os_type                           = "Linux"
  subscription_id                   = var.azure_subscription_id
  tenant_id                         = var.azure_tenant_id
  vm_size                           = "Standard_D4s_v4"
}

build {
  name="wil"
  sources = ["source.azure-arm.jenkins"]

  provisioner "file" {
    destination = "/tmp"
    source      = "${var.jenkins_ext_dir}/ssh_keys"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/jenkins/conf"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/jenkins/data"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/jenkins/scripts"
  }

  provisioner "shell" {
    environment_vars = [
      "EMAIL_PWD=${var.email_pwd}",
      "JENKINS_USER_NAME=${var.jenkins_user_name}",
      "JENKINS_USER_PWD=${var.jenkins_user_pwd}",
      "NEXUS_JENKINS_PWD=${var.nexus_jenkins_pwd}",
      "BITBUCKET_JENKINS_PWD=${var.bitbucket_jenkins_pwd}",
      "JENKINS_PRIVATE_KEY_PASSPHRASE=${var.jenkins_private_key_passphrase}"
    ]
    execute_command  = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "../../provisioning/jenkins/setup.sh"
  }

}
