variable "location" {
  type        = string
  description = "The location where resources are created"
  default     = "West Europe"
}

variable "images_resource_group_name" {
  type        = string
  description = "The name of the resource group in which the images are created"
  default     = "images"
}

variable "prefix" {
  type        = string
  description = "The prefix that we will use for resource naming"
  default     = "wil"
}

variable "project_name" {
  type        = string
  description = "The name that we will use as the 'project' tag value"
}

variable "azure_subscription_id" {
  sensitive   = true
  type        = string
  description = "The id of the Azure subscription"
}

variable "azure_tenant_id" {
  sensitive   = true
  type        = string
  description = "The id of the Azure tenant"
}

variable "azure_client_id" {
  sensitive   = true
  type        = string
  description = "The id of the Azure client"
}

variable "azure_client_secret" {
  sensitive   = true
  type        = string
  description = "The secret of the Azure client"
}

variable "email_pwd" {
  sensitive   = true
  type        = string
  description = "The password for email smtp gateway"
}

variable "aws_nexus_access_key_id" {
  sensitive   = true
  type        = string
  description = "The AWS access key Id for Nexus Repository Manager"
}

variable "aws_nexus_access_key_secret" {
  sensitive   = true
  type        = string
  description = "The AWS access key secret for Nexus Repository Manager"
}

variable "bitbucket_jenkins_pwd" {
  sensitive   = true
  type        = string
  description = "The password for Jenkins access to BitBucket"
}

variable "nexus_admin_pwd" {
  sensitive   = true
  type        = string
  description = "The administrator's password fo Nexus"
}

variable "nexus_jenkins_pwd" {
  sensitive   = true
  type        = string
  description = "The Jenkins' password fo Nexus"
}

variable "sonarqube_admin_pwd" {
  sensitive   = true
  type        = string
  description = "The administrator's password fo Sonarqube"
}

variable "sonarqube_jenkins_pwd" {
  sensitive   = true
  type        = string
  description = "The Jenkins' password fo Sonarqube"
}


variable "jenkins_user_name" {
  type        = string
  description = "???"
}

variable "jenkins_user_pwd" {
  sensitive   = true
  type        = string
  description = "???"
}

variable "jenkins_ext_dir" {
  type        = string
  description = "???"
}

variable "jenkins_private_key_passphrase" {
  sensitive   = true
  type        = string
  description = "???"
}
