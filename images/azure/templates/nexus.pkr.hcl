source "azure-arm" "nexus" {
  azure_tags = {
    Project = var.project_name
  }
  client_id                         = var.azure_client_id
  client_secret                     = var.azure_client_secret
  image_offer                       = "0001-com-ubuntu-server-jammy"
  image_publisher                   = "Canonical"
  image_sku                         = "22_04-lts-gen2"
  location                          = "${var.location}"
  managed_image_name                = "${var.prefix}-nexus"
  managed_image_resource_group_name = var.images_resource_group_name
  os_type                           = "Linux"
  subscription_id                   = var.azure_subscription_id
  tenant_id                         = var.azure_tenant_id
  vm_size                           = "Standard_D4s_v4"
}

build {
  name    = "wil"
  sources = ["source.azure-arm.nexus"]

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/nexus/conf"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/nexus/scripts"
  }

  provisioner "shell" {
    environment_vars = [
      "EMAIL_PWD=${var.email_pwd}",
      "NEXUS_ADMIN_PWD=${var.nexus_admin_pwd}",
      "NEXUS_JENKINS_PWD=${var.nexus_jenkins_pwd}",
      "AWS_ACCESS_KEY_ID=${var.aws_nexus_access_key_id}",
      "AWS_ACCESS_KEY_SECRET=${var.aws_nexus_access_key_secret}"
    ]
    execute_command = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "../../provisioning/nexus/setup.sh"
  }

}
