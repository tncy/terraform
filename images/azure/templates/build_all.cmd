@echo off
cls
del /Q *.log
set PACKER_LOG=1

echo Build Reverse Proxy image
set PACKER_LOG_PATH=packer.log
packer build .
