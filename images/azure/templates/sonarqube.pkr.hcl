source "azure-arm" "sonarqube" {
  azure_tags = {
    Project = var.project_name
  }
  client_id                         = var.azure_client_id
  client_secret                     = var.azure_client_secret
  image_offer                       = "0001-com-ubuntu-server-jammy"
  image_publisher                   = "Canonical"
  image_sku                         = "22_04-lts-gen2"
  location                          = "${var.location}"
  managed_image_name                = "${var.prefix}-sonarqube"
  managed_image_resource_group_name = var.images_resource_group_name
  os_type                           = "Linux"
  subscription_id                   = var.azure_subscription_id
  tenant_id                         = var.azure_tenant_id
  vm_size                           = "Standard_D2s_v4"
}

build {
  name    = "wil"
  sources = ["source.azure-arm.sonarqube"]

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/sonarqube/conf"
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "../../provisioning/sonarqube/scripts"
  }

  provisioner "shell" {
    environment_vars = [
      "EMAIL_PWD=${var.email_pwd}",
      "SONARQUBE_JENKINS_PWD=${var.sonarqube_jenkins_pwd}",
      "SONARQUBE_ADMIN_PWD==${var.sonarqube_admin_pwd}"
    ]
    execute_command = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "../../provisioning/sonarqube/setup.sh"
  }

}
