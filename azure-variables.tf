variable "project_name" {
  type        = string
  description = "The name that we will use as the 'project' tag value"
  default     = "Workshop IL"
}

variable "location" {
  type        = string
  description = "The location where resources are created"
  default     = "westeurope"
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which the resources are created"
  default     = "workshop-2023"
}

variable "prefix" {
  type        = string
  description = "The prefix that we will use for resource naming"
  default     = "wil2023"
}

variable "admin_user" {
  type        = string
  description = "The username that we will use for VM administrator (Value get from environment variables)"
}

variable "admin_private_key_path" {
  type        = string
  description = "The local path to the private key file that we will use to connect admin user to VMs  (Value get from environment variables)"
}

variable "admin_public_key_path" {
  type        = string
  description = "The local path to the public key file that we will install in the authorized SSH keys of the VMs for the administrator's connection to the VMs. (Value get from environment variables)"
}